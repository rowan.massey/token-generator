import argparse
import sys
from jose import jws, jwe
from jose.constants import Algorithms

ENCODING = 'utf-8'
SECRET = "625223712dc63bf057b375837a438421683bd444fd6bf34d76f8399239faa93e"

parser = argparse.ArgumentParser(description = "Project", fromfile_prefix_chars='@')
parser.add_argument("--user", "-u", help="User ID")
parser.add_argument("--client", "-c", help="Client ID")
parser.add_argument("--secret", "-s", help="Secret")
args = parser.parse_args()


def create_jwt_token(token_payload: dict, token_secret=None):
    """Verified the token by decrypting it using the secret key"""
    secret = _decode_secret_key(token_secret)
    payload = _generate_payload(token_payload)
    signed = jws.sign(payload, secret, algorithm=Algorithms.HS256)

    jwe_ = jwe.encrypt(plaintext=signed, key=secret, algorithm=Algorithms.DIR, encryption=Algorithms.A128CBC_HS256)

    return str(jwe_.decode(ENCODING))


def _decode_secret_key(secret_key):
    """Converts secret key from hex to bytes"""
    if not str(secret_key).strip():
        raise Exception('Empty secret key. Unable to decode secret.')
    return bytes.fromhex(str(secret_key).strip())


def _generate_payload(token_payload):
    return {
        "clientId": token_payload.get('client_id'),
        "userId": token_payload.get('user_id'),
        "exp": token_payload.get('expires'),
        "consumerId": token_payload.get('consumer_id'),
        "impersonatorClientId": token_payload.get('impersonator_client_id'),
        "impersonatorUserId": token_payload.get('impersonator_user_id'),
        "privilege": token_payload.get('privilege')
    }


if __name__ == '__main__':
    usage = "python3 token_generator.py --user 12345 --client 5780 [--secret 09384ljdsafl2309423lkdjs83423094nv039284vn2323094]"
    user = args.user if args.user else sys.exit(f"Missing User ID: {usage}") # 17601172
    client = args.client if args.client else sys.exit(f"Missing Client ID: {usage}") # 5780
    secret = args.secret if args.secret else SECRET
    print(f"User: {user}")
    print(f"Client: {client}")
    token = create_jwt_token({
        'client_id': int(client),
        'user_id': int(user),
        'expires': 2554416000000,  # Mon Dec 12 2050 00:00:00
        'privilege': 29611601362948210
    }, secret)
    print(f"Bearer {token}")
